// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Library for encodings related to the Tezos SCORU system.
#![cfg_attr(not(feature = "alloc"), no_std)]
#![deny(missing_docs)]
#![deny(rustdoc::all)]
#![forbid(unsafe_code)]

#[cfg(feature = "alloc")]
extern crate alloc;

pub mod dac;
