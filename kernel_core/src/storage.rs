// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Transaction kernel storage
//!
//! The TX kernel allows for batching operations into transactions. If any
//! operation in a transaction fails, the effect of the entire transaction
//! is rolled back. This module allows for both dealing with transactions
//! and the updates to accounts that happen within said transactions.

use crate::bls::PublicKey;
use crate::encoding::string_ticket::StringTicketHash;
use host::path::{concat, OwnedPath, RefPath};
use host::rollup_core::RawRollupCore;
use host::runtime::RuntimeError::PathNotFound;
use host::runtime::{load_value_slice, Runtime};
use tezos_rollup_storage::storage::Storage;
use thiserror::Error;

extern crate alloc;

/// All errors that may happen as result of using this account storage
/// interface.
#[derive(Error, Copy, Eq, PartialEq, Clone, Debug)]
pub enum AccountStorageError {
    /// Tried to take amount of ticket from an account, but the account
    /// does not hold enough funds for this operation.
    #[error("Account does not hold enough funds")]
    NotEnoughFunds,
    /// Some runtime error happened while using the hosts durable storage.
    #[error("Runtime error")]
    RuntimeError(host::runtime::RuntimeError),
    /// Some error happened when constructing the path to some resource
    /// associated with an account.
    #[error("Path error")]
    PathError(host::path::PathError),
    /// Some value (amount or counter) was kept in a malformed format in
    /// durable storage.
    #[error("Malformed 64 bit integer in storage")]
    MalformedValue,
    /// Some signature in storage had a malformed format.
    #[error("Malformed compressed BLS signature")]
    MalformedSignature,
    /// Some error happened in the storage API when creating the account
    /// storage object.
    #[error("Storage API error")]
    StorageError(tezos_rollup_storage::StorageError),
    /// Adding a ticket to an account overflowed an unsigned 64 bit
    /// integer
    #[error("Ticket amount overflow")]
    AmountOverflow,
    /// A 64 bit counter for an account overflowed.
    #[error("Counter overflow")]
    CounterOverflow,
}

impl From<tezos_rollup_storage::StorageError> for AccountStorageError {
    fn from(error: tezos_rollup_storage::StorageError) -> Self {
        AccountStorageError::StorageError(error)
    }
}

impl From<host::path::PathError> for AccountStorageError {
    fn from(error: host::path::PathError) -> Self {
        AccountStorageError::PathError(error)
    }
}

impl From<host::runtime::RuntimeError> for AccountStorageError {
    fn from(error: host::runtime::RuntimeError) -> Self {
        AccountStorageError::RuntimeError(error)
    }
}

/// A TX kernel account
///
/// The account is stored in `path` held by the Account structure (private
/// field). Sub paths include:
/// - the `counter` field. A 64 bit signed integer
/// - the `public_key` field. A 48 byte comressed BLS public key.
/// - a sub path for each ticket held by the account. Ticket paths are
///   named by the hex encoded hash of the ticket. Each such path holds
///   the amount of such tickets held by the account encoded as unsigned
///   64 bit integers.
///
/// If a path is absent (counter- or ticket- path), then the value defaults
/// to zero. If the public key path doesn't contain anything, it is given as
/// `None` - otherwise it will contain "Some" compressed BLS public key.
#[derive(Debug, PartialEq)]
pub struct Account {
    path: OwnedPath,
}

/// The sub-path for holding the account counter value.
const COUNTER_PATH: RefPath = RefPath::assert_from(b"/counter");

/// The sub-path for the accounts public key.
const PUBLIC_KEY_PATH: RefPath = RefPath::assert_from(b"/public.key");

impl Account {
    /// Get the full path to where the amount of a specific ticket for the account
    /// is stored.
    fn ticket_path(
        &self,
        ticket_hash: &StringTicketHash,
    ) -> Result<OwnedPath, AccountStorageError> {
        let sth_path_name: Vec<u8> = alloc::format!("/{}", ticket_hash).into();
        let sth_path = OwnedPath::try_from(sth_path_name)?;

        concat(&self.path, &sth_path).map_err(AccountStorageError::from)
    }

    /// Get amount of given ticket held by the account.
    ///
    /// If there is not value in durable storage for this account and the
    /// ticket identified by the hash, then the value defaults to zero.
    pub fn ticket_amount<Host: Runtime + RawRollupCore>(
        &self,
        host: &Host,
        ticket_hash: &StringTicketHash,
    ) -> Result<Option<u64>, AccountStorageError> {
        let path = self.ticket_path(ticket_hash)?;

        let mut buffer = [0_u8; 8];

        match load_value_slice(host, &path, &mut buffer) {
            Ok(8) => {
                let value = u64::from_le_bytes(buffer);
                Ok(if value != 0_u64 { Some(value) } else { None })
            }
            Err(PathNotFound) => Ok(None),
            _ => Err(AccountStorageError::MalformedValue),
        }
    }

    /// Take number of tickets from account
    ///
    /// This function is made to match the use case of transactions. TX kernel
    /// transactions are given as unsigned 64 bit integers for the amount transferred,
    /// so such transactions require two calls: one for the debit and one for the credit).
    /// This function is for the credit.
    pub fn remove_ticket<Host: Runtime + RawRollupCore>(
        &mut self,
        host: &mut Host,
        ticket_hash: &StringTicketHash,
        amount: u64,
    ) -> Result<(), AccountStorageError> {
        let path = self.ticket_path(ticket_hash)?;

        let mut buffer = [0_u8; 8];

        match load_value_slice(host, &path, &mut buffer) {
            Ok(8) => {
                let old_amount = u64::from_le_bytes(buffer);

                match old_amount.checked_sub(amount) {
                    Some(0) => {
                        host.store_delete(&path).map_err(AccountStorageError::from)
                    }
                    Some(new_amount) => host
                        .store_write(&path, &new_amount.to_le_bytes(), 0)
                        .map_err(AccountStorageError::from),
                    None => Err(AccountStorageError::NotEnoughFunds),
                }
            }
            Err(PathNotFound) => {
                if amount != 0 {
                    Err(AccountStorageError::NotEnoughFunds)
                } else {
                    Ok(())
                }
            }
            Err(error) => Err(AccountStorageError::from(error)),
            Ok(_) => Err(AccountStorageError::MalformedValue),
        }
    }

    /// Give number of tickets to the account
    ///
    /// This function is made to match the use case of transactions. TX kernel
    /// transactions are given as unsigned 64 bit integers for the amount transferred,
    /// so such transactions require two calls: one for the debit and one for the
    /// credit). This function is for the debit.
    pub fn add_ticket<Host: Runtime + RawRollupCore>(
        &mut self,
        host: &mut Host,
        ticket_hash: &StringTicketHash,
        amount: u64,
    ) -> Result<(), AccountStorageError> {
        let path = self.ticket_path(ticket_hash)?;

        let mut buffer = [0_u8; 8];

        match load_value_slice(host, &path, &mut buffer) {
            Ok(8) => {
                let old_amount = u64::from_le_bytes(buffer);

                if let Some(new_value) = old_amount.checked_add(amount) {
                    host.store_write(&path, &new_value.to_le_bytes(), 0)
                        .map_err(AccountStorageError::from)
                } else {
                    Err(AccountStorageError::AmountOverflow)
                }
            }
            Err(PathNotFound) => host
                .store_write(&path, &amount.to_le_bytes(), 0)
                .map_err(AccountStorageError::from),
            Ok(_) => Err(AccountStorageError::MalformedValue),
            Err(error) => Err(AccountStorageError::from(error)),
        }
    }

    /// Get the current counter value for the account
    ///
    /// If the store doesn't hold a counter value for the account,
    /// then the counter value is assumed to be zero.
    pub fn counter<Host: Runtime + RawRollupCore>(
        &self,
        host: &Host,
    ) -> Result<i64, AccountStorageError> {
        let path = concat(&self.path, &COUNTER_PATH)?;

        let mut buffer = [0_u8; 8];

        match load_value_slice(host, &path, &mut buffer) {
            Ok(8) => Ok(i64::from_le_bytes(buffer)),
            Ok(_) => Err(AccountStorageError::MalformedValue),
            Err(PathNotFound) => Ok(0_i64),
            Err(error) => Err(AccountStorageError::from(error)),
        }
    }

    /// Increment the counter for the account
    ///
    /// If the store doesn't hold a value for the counter it is assumed to be
    /// zero before increment, so it will be stored as one after this call. Otherwise
    /// increment the value stored by one. This may overflow, at least in theory.
    pub fn increment_counter<Host: Runtime + RawRollupCore>(
        &mut self,
        host: &mut Host,
    ) -> Result<(), AccountStorageError> {
        let path = concat(&self.path, &COUNTER_PATH)?;

        let mut buffer = [0_u8; 8];

        match load_value_slice(host, &path, &mut buffer) {
            Ok(8) => {
                let old_value = i64::from_le_bytes(buffer);
                if let Some(new_value) = old_value.checked_add(1) {
                    host.store_write(&path, &new_value.to_le_bytes(), 0)
                        .map_err(AccountStorageError::from)
                } else {
                    Err(AccountStorageError::CounterOverflow)
                }
            }
            Ok(_) => Err(AccountStorageError::MalformedValue),
            Err(PathNotFound) => host
                .store_write(&path, &(1_i64).to_le_bytes(), 0)
                .map_err(AccountStorageError::from),
            Err(error) => Err(AccountStorageError::from(error)),
        }
    }

    /// Link a public key to the account
    ///
    /// In case the account already has a key, the new key will replace it.
    /// No check is performed to see if the public key corresponds to the
    /// account address.
    pub fn link_public_key<Host: Runtime + RawRollupCore>(
        &mut self,
        host: &mut Host,
        pk: &PublicKey,
    ) -> Result<(), AccountStorageError> {
        let path = concat(&self.path, &PUBLIC_KEY_PATH)?;

        let buffer: [u8; 48] = pk.clone().into();

        host.store_write(&path, &buffer, 0)
            .map_err(AccountStorageError::from)
    }

    /// Disassociate a public key from an account
    ///
    /// In case the account has no key associated with it, nothing happens.
    /// Othwerise any previously created link will be removed.
    pub fn unlink_public_key<Host: Runtime + RawRollupCore>(
        &mut self,
        host: &mut Host,
    ) -> Result<(), AccountStorageError> {
        let path = concat(&self.path, &PUBLIC_KEY_PATH)?;

        match host.store_delete(&path) {
            Ok(_) => Ok(()),
            Err(PathNotFound) => Ok(()),
            Err(error) => Err(AccountStorageError::from(error)),
        }
    }

    /// Get the public key for an account if it exists
    ///
    /// An account doesn't necessarily have a public key associated with it.
    pub fn public_key<Host: Runtime + RawRollupCore>(
        &self,
        host: &Host,
    ) -> Result<Option<PublicKey>, AccountStorageError> {
        let path = concat(&self.path, &PUBLIC_KEY_PATH)?;

        let mut buffer = [0_u8; 48];

        match load_value_slice(host, &path, &mut buffer) {
            Ok(48) => Ok(Some(
                PublicKey::try_from(buffer)
                    .map_err(|_| AccountStorageError::MalformedSignature)?,
            )),
            Ok(_) => Err(AccountStorageError::MalformedSignature),
            Err(PathNotFound) => Ok(None),
            Err(error) => Err(AccountStorageError::from(error)),
        }
    }
}

impl From<OwnedPath> for Account {
    fn from(path: OwnedPath) -> Self {
        Self { path }
    }
}

/// Path where world state of accounts are stored
const ACCOUNTS_PATH: RefPath = RefPath::assert_from(b"/accounts");

/// The type of storage used for TX kernel accounts.
pub type AccountStorage = Storage<Account>;

/// Get the initial storage interface for accounts
pub fn init_account_storage() -> Result<AccountStorage, AccountStorageError> {
    Storage::<Account>::init(&ACCOUNTS_PATH).map_err(AccountStorageError::from)
}

#[cfg(test)]
mod test {
    use crate::bls::{BlsKey, PublicKey};
    use crate::encoding::contract::Contract;
    use crate::encoding::string_ticket::StringTicket;
    use crate::storage::init_account_storage;
    use host::path::RefPath;
    use mock_runtime::host::MockHost;

    #[test]
    fn test_account_counter_update() {
        let mut host = MockHost::default();
        let mut storage =
            init_account_storage().expect("Could not create accounts storage API");

        let a1_path = RefPath::assert_from(b"/asdf");

        // Act
        storage
            .begin_transaction(&mut host)
            .expect("Could not begin transaction");

        let mut a1 = storage
            .new_account(&mut host, &a1_path)
            .expect("Could not create new account")
            .expect("Account already exists in storage");

        assert_eq!(
            a1.counter(&host)
                .expect("Could not get counter for account"),
            0_i64
        );

        a1.increment_counter(&mut host)
            .expect("Could not increment counter");

        storage
            .commit(&mut host)
            .expect("Could not commit transaction");

        // Assert
        let a1 = storage
            .get_account(&mut host, &a1_path)
            .expect("Could not get account")
            .expect("Account does not exist");

        assert_eq!(
            a1.counter(&host)
                .expect("Could not get counter for account"),
            1_i64
        );
    }

    #[test]
    fn test_account_ticket_deposit() {
        let mut host = MockHost::default();
        let mut storage =
            init_account_storage().expect("Could not create accounts storage API");

        let a1_path = RefPath::assert_from(b"/kjfds");
        let tz1 = "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx";
        let contract = Contract::from_b58check(tz1).expect("expected valid tz1 hash");
        let ticket = StringTicket::new(contract, "blah".to_string(), 100);
        let ticket_hash = ticket.identify().expect("Could not identify test ticket");

        // Act
        storage
            .begin_transaction(&mut host)
            .expect("Could not begin transaction");

        let mut a1 = storage
            .new_account(&mut host, &a1_path)
            .expect("Could not create new account")
            .expect("Account already exists in storage");

        assert_eq!(
            a1.ticket_amount(&host, &ticket_hash)
                .expect("Could not get ticket amount"),
            None
        );

        a1.add_ticket(&mut host, &ticket_hash, 10)
            .expect("Could not give ticket to account");

        storage
            .commit(&mut host)
            .expect("Could not commit transaction");

        // Assert
        let a1 = storage
            .get_account(&mut host, &a1_path)
            .expect("Could not get account")
            .expect("Account does not exist");

        assert_eq!(
            a1.ticket_amount(&host, &ticket_hash)
                .expect("Could not get ticket amount"),
            Some(10)
        );
    }

    #[test]
    fn test_account_ticket_fund_limit() {
        let mut host = MockHost::default();
        let mut storage =
            init_account_storage().expect("Could not create accounts storage API");

        let a1_path = RefPath::assert_from(b"/asdf");
        let a2_path = RefPath::assert_from(b"/fdsa");
        let tz1 = "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx";
        let contract = Contract::from_b58check(tz1).expect("expected valid tz1 hash");
        let ticket = StringTicket::new(contract, "blah".to_string(), 100);
        let ticket_hash = ticket.identify().expect("Could not identify test ticket");

        // Act
        storage
            .begin_transaction(&mut host)
            .expect("Could not begin transaction");

        let mut a1 = storage
            .new_account(&mut host, &a1_path)
            .expect("Could not create new account")
            .expect("Account already exists");

        let mut a2 = storage
            .new_account(&mut host, &a2_path)
            .expect("Could not create new account")
            .expect("Account already exists");

        a1.add_ticket(&mut host, &ticket_hash, 10)
            .expect("Could not give 10 tickets to account");
        a2.add_ticket(&mut host, &ticket_hash, 20)
            .expect("Could not give 20 tickets to account");

        // Assert
        a1.remove_ticket(&mut host, &ticket_hash, 11)
            .expect_err("Wasn't supposed to be able to take 11 tickets from an account that has only 10");

        // Act some more...
        storage
            .rollback(&mut host)
            .expect("Could not rollback transaction");

        // Assert again
        let a1 = storage
            .get_account(&mut host, &a1_path)
            .expect("Could not get account");

        assert_eq!(a1, None);

        let a2 = storage
            .get_account(&mut host, &a2_path)
            .expect("Could not get account");

        assert_eq!(a2, None);
    }

    #[test]
    fn test_public_key() {
        let mut host = MockHost::default();
        let mut storage =
            init_account_storage().expect("Could not create accounts storage API");

        let a1_path = RefPath::assert_from(b"/asdf");
        let a1_public_key: PublicKey = BlsKey::from_ikm([1_u8; 32]).public_key().clone();

        // Make sure there is an account
        storage
            .new_account(&mut host, &a1_path)
            .expect("Could not create account")
            .expect("No account created")
            .increment_counter(&mut host)
            .expect("Could not increment counter of new account");

        // Assert that there is no public key initially
        assert_eq!(
            storage
                .get_account(&mut host, &a1_path)
                .expect("Could not read account")
                .expect("No account found")
                .public_key(&host)
                .expect("Error when getting public key for account"),
            None
        );

        // Associate a public key with the account
        storage
            .begin_transaction(&mut host)
            .expect("Could not begin transaction");

        storage
            .get_account(&mut host, &a1_path)
            .expect("Could not get account")
            .expect("No account found")
            .link_public_key(&mut host, &a1_public_key)
            .expect("Could not link public key to account");

        // Assert that public key has been updated
        assert_eq!(
            storage
                .get_account(&mut host, &a1_path)
                .expect("Could not read account")
                .expect("No account found")
                .public_key(&host)
                .expect("Could not get public key from storage"),
            Some(a1_public_key.clone())
        );

        storage
            .commit(&mut host)
            .expect("Could not commit transaction");

        // Assert that public key update has been committed
        assert_eq!(
            storage
                .get_account(&mut host, &a1_path)
                .expect("Could not read account")
                .expect("No account found")
                .public_key(&host)
                .expect("Could not get public key from storage"),
            Some(a1_public_key.clone())
        );
    }

    #[test]
    fn test_public_key_rollback() {
        let mut host = MockHost::default();
        let mut storage =
            init_account_storage().expect("Could not create accounts storage API");

        let a1_path = RefPath::assert_from(b"/asdf");
        let a1_public_key: PublicKey = BlsKey::from_ikm([1_u8; 32]).public_key().clone();

        // Make sure there is an account
        storage
            .new_account(&mut host, &a1_path)
            .expect("Could not create account")
            .expect("No account created")
            .increment_counter(&mut host)
            .expect("Could not increment counter of new account");

        // Assert that there is no public key initially
        assert_eq!(
            storage
                .get_account(&mut host, &a1_path)
                .expect("Could not read account")
                .expect("No account found")
                .public_key(&host)
                .expect("Error when getting public key for account"),
            None
        );

        // Associate a public key with the account
        storage
            .begin_transaction(&mut host)
            .expect("Could not begin transaction");

        storage
            .get_account(&mut host, &a1_path)
            .expect("Could not get account")
            .expect("No account found")
            .link_public_key(&mut host, &a1_public_key)
            .expect("Could not link public key to account");

        // Assert that public key has been updated
        assert_eq!(
            storage
                .get_account(&mut host, &a1_path)
                .expect("Could not read account")
                .expect("No account found")
                .public_key(&host)
                .expect("Could not get public key from storage"),
            Some(a1_public_key.clone())
        );

        storage
            .rollback(&mut host)
            .expect("Could not rollback transaction");

        // Assert that public key update has been committed
        assert_eq!(
            storage
                .get_account(&mut host, &a1_path)
                .expect("Could not read account")
                .expect("No account found")
                .public_key(&host)
                .expect("Could not get public key from storage"),
            None
        );
    }
}
