// SPDX-FileCopyrightText: 2022 Nomadic Labs <contact@nomadic-labs.com>
//
// SPDX-License-Identifier: MIT

//! Hash of Layer1 smart rollup addresses.

use tezos_encoding::enc::BinWriter;
use tezos_encoding::encoding::HasEncoding;
use tezos_encoding::nom::NomReader;

use crypto::base58::{FromBase58Check, FromBase58CheckError};
use crypto::hash::{HashType, SmartRollupHash};

/// Smart rollup address
#[derive(Debug, Clone, PartialEq, Eq, HasEncoding, BinWriter, NomReader)]
pub struct SmartRollupAddress {
    hash: SmartRollupHash,
}

impl SmartRollupAddress {
    /// Conversion from base58-encoding string (with scr1 prefix).
    pub fn from_b58check(data: &str) -> Result<Self, FromBase58CheckError> {
        let bytes = data.from_base58check()?;

        if bytes.starts_with(HashType::SmartRollupHash.base58check_prefix()) {
            let hash = SmartRollupHash::from_base58_check(data)?;
            Ok(SmartRollupAddress { hash })
        } else {
            Err(FromBase58CheckError::InvalidBase58)
        }
    }

    /// Conversion to base58-encoding string (with scr1 prefix).
    pub fn to_b58check(&self) -> String {
        self.hash.to_base58_check()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn scr1_b58check() {
        let scr1 = "scr1HLXM32GacPNDrhHDLAssZG88eWqCUbyLF";

        let smart_rollup_address = SmartRollupAddress::from_b58check(scr1);

        assert!(matches!(smart_rollup_address, Ok(_)));

        let scr1_from_smart_rollup_address = smart_rollup_address.unwrap().to_b58check();

        assert_eq!(scr1, &scr1_from_smart_rollup_address);
    }

    #[test]
    fn scr1_encoding() {
        let scr1 = "scr1HLXM32GacPNDrhHDLAssZG88eWqCUbyLF";

        let smart_rollup_address =
            SmartRollupAddress::from_b58check(scr1).expect("expected valid scr1 hash");

        let mut bin = Vec::new();
        smart_rollup_address
            .bin_write(&mut bin)
            .expect("serialization should work");

        let (remaining_input, deserde_scr1) =
            SmartRollupAddress::nom_read(bin.as_slice())
                .expect("deserialization should work");

        assert!(remaining_input.is_empty());
        assert_eq!(smart_rollup_address, deserde_scr1);
    }
}
