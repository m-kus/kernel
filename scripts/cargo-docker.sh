# SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
#
# SPDX-License-Identifier: MIT

docker build . -t wasm-kernel

cargo(){
  tty_arg='-it'
   
  if [ -z "$TTY" ]; then
    tty_arg=''
  fi
  
  docker run -v $PWD:/usr/src -v $HOME/.cargo:/usr/cargo -u $(id -u ${USER}):$(id -g ${user}) \
      --rm $tty_arg --name wasm-kernel wasm-kernel:latest 'cargo' "$@"
}
