// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Types & encodings for the *outbox-half* of the *L1/L2 communication protocol*
//!
//! This module is a wrapper for the transaction kernel outbox. It will eventually
//! contain EVM specific outbox messages as well (this is a TODO that is out of
//! scope for the first iteration).

/// Transaction kernel outbox types and functions
pub mod tx {
    pub use kernel_core::outbox::*;
}
