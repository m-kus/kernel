// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Verifiable and verified transactions
//!
//! This module contains both verified versions of ticket transactions, ie,
//! transactions similar to transaction kernel ones, and also verified/verifiable
//! Ethereum transactions.
use crate::inbox::v1::ParsedEvmTransaction;
use thiserror::Error;

/// Erros from either a failed TX kernel transaction or EVM contract call
#[derive(Debug, Error)]
pub enum VerificationError {
    /// EVMcall failed
    /// TODO: <https://gitlab.com/tezos/tezos/-/issues/3549>
    #[error("EVM call failed")]
    EVMCallFailure,
    // Add TX kernel failures to this enum
    // TODO: <https://gitlab.com/tezos/tezos/-/issues/3698>
}

/// A transaction that can be safely applied
///
/// In case of ticket transactions, this means all operations can be
/// executed and that the signature is correct. In case of an Ethereum
/// transaction, this is the _result_ of executing the transaction, ie,
/// running the EVM contract (or creating it and running the constructor),
/// has already been done and was succesful, but the effect of the
/// transaction will be contained in this structure.
/// TODO: <https://gitlab.com/tezos/tezos/-/issues/3549>
pub enum VerifiedTransaction {
    /// A ticket transaction
    TicketTransaction(super::tx::verifiable::VerifiedTransaction),
    /// An Ethereum transaction - signature has been verified
    EthereumTransaction(ParsedEvmTransaction),
}
