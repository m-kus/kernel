// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Types and functions for Ethereum compatibility
//!
//! We need to read and write Ethereum specific values such
//! as addresses and values.
use alloc::rc::Rc;
use evm::executor::stack::PrecompileFailure;
use evm::{Capture, Config, Context, ExitError, ExitFatal, ExitReason, Handler, Resolve};
use host::rollup_core::RawRollupCore;
use primitive_types::{H160, U256};
use thiserror::Error;

pub mod address;
pub mod basic;
pub mod block;
pub mod handler;
pub mod precompiles;
pub mod storage;
#[cfg(feature = "testing")]
pub mod testing;
pub mod transaction;

use precompiles::PrecompileSet;

/// Errors when calling EVM contracts
///
/// What could possibly go wrong? Some of these are place holders for now.
/// When we call an address without code, this should be treated as a simple
/// transfer for instance.
/// TODO: pertty print error messages inclduing arguments
#[derive(Error, Debug, Eq, PartialEq)]
pub enum EvmCallError {
    /// A contract was called and the contract doesn't exist.
    #[error("No contract at address: {0}")]
    NoContractAtAddress(H160),
    /// EVM returned with a machine error
    #[error("Internal machine error when running contract")]
    MachineExitError(ExitError),
    /// A fatal error from executing EVM.
    #[error("Fatal machine error when running contract")]
    FatalMachineError(ExitFatal),
    /// Calling a precompiled failed (implies there was a precompiled contract
    /// at the call address.
    #[error("Precompile call failed")]
    PrecompileFailed(PrecompileFailure),
    /// Contract did revert
    #[error("Contract call reverted")]
    CallRevert,
}

/// Call the EVM
pub fn call_evm<'a, Host>(
    host: &'a mut Host,
    block: &'a block::BlockConstants,
    precompiles: &'a precompiles::PrecompileBTreeMap<Host>,
    address: H160,
    caller: H160,
    call_data: &[u8],
) -> Result<(), EvmCallError>
where
    Host: RawRollupCore,
{
    let context = Context {
        address,
        caller,
        apparent_value: U256::zero(),
    };
    let dummy_gas_limit = None;
    let dummy_is_static = false;
    let mut handler = handler::EvmHandler::<'a, Host>::new(host, block, context.clone());

    if handler.exists(address) {
        let code = Rc::new(handler.code(address));
        let data: Rc<Vec<u8>> = Rc::new(Vec::from(call_data));
        let config = Config::frontier();
        let mut runtime = evm::Runtime::new(code, data, context, &config);

        let result = match runtime.run(&mut handler) {
            Capture::Exit(ExitReason::Succeed(_exit_succeed)) => {
                Ok(()) // TODO return the output data
            }
            Capture::Exit(ExitReason::Error(error)) => {
                Err(EvmCallError::MachineExitError(error))
            }
            Capture::Exit(ExitReason::Revert(_exit_revert)) => {
                Err(EvmCallError::CallRevert)
            }
            Capture::Exit(ExitReason::Fatal(fatal_error)) => {
                Err(EvmCallError::FatalMachineError(fatal_error))
            }
            Capture::Trap(Resolve::Create(_handler, _config)) => {
                todo!("Create contract")
            }
            Capture::Trap(Resolve::Call(_handler, _config)) => {
                todo!("contract call contract")
            }
        };

        result
    } else if let Some(precompile_result) = precompiles.execute(
        &mut handler,
        address,
        call_data,
        dummy_gas_limit,
        &context,
        dummy_is_static,
    ) {
        match precompile_result {
            Ok(_) => Ok(()),
            Err(e) => Err(EvmCallError::PrecompileFailed(e)),
        }
    } else {
        Err(EvmCallError::NoContractAtAddress(address))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use evm::Opcode;
    use mock_runtime::host::MockHost;
    use primitive_types::H160;

    #[test]
    fn call_non_existing_contract() {
        // Arange
        let mut mock_runtime = MockHost::default();
        let block = block::BlockConstants::first_block();
        let precompiles = precompiles::precompile_set::<MockHost>();
        let target = H160::from_low_u64_be(117u64);
        let caller = H160::from_low_u64_be(118u64);
        let data = [0u8; 0];

        // Act
        let result = call_evm(
            &mut mock_runtime,
            &block,
            &precompiles,
            target,
            caller,
            &data,
        );

        // Assert
        let expected_result = Err(EvmCallError::NoContractAtAddress(target));
        assert_eq!(result, expected_result);
    }

    #[test]
    fn call_simple_return_contract() {
        // Arrange
        let mut mock_runtime = MockHost::default();
        let block = block::BlockConstants::first_block();
        let precompiles = precompiles::precompile_set::<MockHost>();
        let target = H160::from_low_u64_be(117u64);
        let caller = H160::from_low_u64_be(118u64);
        let data = [0u8; 0];
        let code = vec![
            Opcode::PUSH1.as_u8(),
            0u8,
            Opcode::PUSH1.as_u8(),
            0u8,
            Opcode::RETURN.as_u8(),
        ];

        storage::set_code(&mut mock_runtime, target, &code);

        // Act
        let result = call_evm(
            &mut mock_runtime,
            &block,
            &precompiles,
            target,
            caller,
            &data,
        );

        // Assert
        let expected_result = Ok(());
        assert_eq!(result, expected_result);
    }

    #[test]
    fn call_simple_revert_contract() {
        // Arrange
        let mut mock_runtime = MockHost::default();
        let block = block::BlockConstants::first_block();
        let precompiles = precompiles::precompile_set::<MockHost>();
        let target = H160::from_low_u64_be(117u64);
        let caller = H160::from_low_u64_be(118u64);
        let data = [0u8; 0];
        let code = vec![
            Opcode::PUSH1.as_u8(),
            0u8,
            Opcode::PUSH1.as_u8(),
            0u8,
            Opcode::REVERT.as_u8(),
        ];

        storage::set_code(&mut mock_runtime, target, &code);

        // Act
        let result = call_evm(
            &mut mock_runtime,
            &block,
            &precompiles,
            target,
            caller,
            &data,
        );

        // Assert
        let expected_result = Err(EvmCallError::CallRevert);
        assert_eq!(result, expected_result);
    }

    #[test]
    fn call_contract_with_invalid_opcode() {
        // Arrange
        let mut mock_runtime = MockHost::default();
        let block = block::BlockConstants::first_block();
        let precompiles = precompiles::precompile_set::<MockHost>();
        let target = H160::from_low_u64_be(117u64);
        let caller = H160::from_low_u64_be(118u64);
        let data = [0u8; 0];
        let code = vec![
            Opcode::INVALID.as_u8(),
            Opcode::PUSH1.as_u8(),
            0u8,
            Opcode::PUSH1.as_u8(),
            0u8,
            Opcode::REVERT.as_u8(),
        ];

        storage::set_code(&mut mock_runtime, target, &code);

        // Act
        let result = call_evm(
            &mut mock_runtime,
            &block,
            &precompiles,
            target,
            caller,
            &data,
        );

        // Assert
        let expected_result =
            Err(EvmCallError::MachineExitError(ExitError::DesignatedInvalid));
        assert_eq!(result, expected_result);
    }

    #[test]
    fn call_precompiled_contract() {
        // Arrange
        let mut mock_runtime = MockHost::default();
        let block = block::BlockConstants::first_block();
        let precompiles = precompiles::precompile_set::<MockHost>();
        let target = H160::from_low_u64_be(7u64);
        let caller = H160::from_low_u64_be(118u64);
        let data = [0u8; 0];

        // Act
        let result = call_evm(
            &mut mock_runtime,
            &block,
            &precompiles,
            target,
            caller,
            &data,
        );

        // Assert
        let expected_result = Ok(());

        assert_eq!(result, expected_result);
    }
}
