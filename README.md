<!--
SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
SPDX-FileCopyrightText: 2022 Nomadic Labs <contact@nomadic-labs.com>

SPDX-License-Identifier: MIT
-->

# Scoru Wasm Dex Kernel

[Docs](https://tezos.gitlab.io/kernel/doc/kernel/index.html).

## Setup
Either native setup or docker are supported.
> The build is confirmed to work on Linux, but there are issues with Mac-M1 when
> targetting `wasm32-unknown-unknown`
>
> Until this is fixed, when developing on Mac you should follow the native
> build steps. All `cargo make` targets which don't have `wasm` in their name
> should work (e.g. `cargo make test`, `cargo make check` should work.)

### Native
To get started, install **rust** using the [https://rustup.rs/](rustup) tool. Rust version **1.60** is required.

You may also need to install `clang`. See the [Dockerfile] for the full list.

You should then be able to add the `wasm` target by running:
```shell
rustup target add wasm32-unknown-unknown
```

### Dockerfile
Alternatively, development using docker can be enabled with:
```shell
source scripts/cargo-docker.sh
```

## Building
To build the crate, run:
```shell
cargo build --target wasm32-unknown-unknown
```

Alternatively use cargo make. First install `cargo-make` using
```shell
cargo install cargo-make
```

Then run
```shell
cargo make wasm-tx-kernel
```

> To build with signature verification disabled (possibly for tests) instead use `wasm-tx-kernel-no-sig-verif`.

You should now see `kernel_core.wasm` under `target/wasm32-unknown-unknown/release` or possibly
`target/wasm32-unknown-unknown/release/deps` depending on how you built things.

# Test kernels
There are test kernels available, both for running computation, and for checking the behaviour of the pvm host functions.

Once built, you will also see a `test_kernel.wasm` under `target/wasm32-unknown-unknown/debug`. It will be under
`../release` if you add the `--release` flag to the cargo call above. To customize the test kernel,
use the feature flags listed in `test_kernel/Cargo.toml`. This is already set up as manual jobs with
Gitlab CI, so a range of test kernels can be build and downloaded from Gitlab.

The test kernels are un-stripped. To strip them and greatly reduce their size, use the `wasm-strip` utility
distributed with [The WebAssembly Binary Toolkit](https://github.com/WebAssembly/wabt). Run
```shell
wasm-strip test_kernel.wasm
```

## Running tests
To run tests for all crates, run:
```shell
cargo test --features testing
```

## Pre-commit
You may want to setup a git pre-commit hook which does the same checks as CI:
```shell
cp scripts/pre-commit.sh .git/hooks/pre-commit
```
